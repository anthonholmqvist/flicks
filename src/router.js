import Vue from 'vue';
import Router from 'vue-router';
import FlicksList from '@/components/FlicksList.vue';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'FlicksList',
      component: FlicksList,
    },
  ],
});

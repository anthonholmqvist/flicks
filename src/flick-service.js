const FILE_ID = '1oCDb58ksiuI-yXql9gpmRvcjYx80peuY01kdgObouHQ';
const MIME_TYPE = 'text/csv';
const API_KEY = 'AIzaSyBDPWy6W6YYf0G-jLOBvYS1rAjycsKwYUk';

async function makeRequest(url) {
  const response = await fetch(url);
  return await response.text();
}

function toListOfFlicks(csv) {
  const flicks = [];
  const lines = csv.split('\n');
  const data = lines.map(data => data.split(',')).filter(column => column[1] !== '');
  data.forEach(item => {
    const [, title, link, aired, dateAired] = item;
    flicks.push({
      title: title.trim(),
      link: link.trim(),
      aired: aired === 'aired' ? true : false,
      dateAired: dateAired.trim() === '' ? 'N/A' : dateAired.trim(),
    });
  });
  return flicks.slice(1);
}

export default {
  async getFlicks() {
    const csv = await makeRequest(
      `https://www.googleapis.com/drive/v3/files/${FILE_ID}/export?mimeType=${MIME_TYPE}&key=${API_KEY}`
    );
    const flicks = toListOfFlicks(csv);
    return flicks;
  },
};

import Vue from 'vue';
import router from '@/router';
import FlicksApp from '@/FlicksApp.vue';
import '@/register-service-worker';
import '@/assets/styles.scss';

Vue.config.productionTip = false;

new Vue({
  router,
  render: h => h(FlicksApp),
}).$mount('#app');
